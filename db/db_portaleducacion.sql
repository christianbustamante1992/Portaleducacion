create table estado(

	estado_id int(5) auto_increment primary key,
    estado_estado int(1) not null,
    estado_nombre varchar(100) not null

);

create table empresa(
    empresa_id int(5) auto_increment primary key,
    empresa_ruc varchar(13) unique not null,
    empresa_nombre varchar(400) not null,
    empresa_direccion varchar(400) not null,
    empresa_telefono varchar(10) not null,
    empresa_celular varchar(10) not null,
    empresa_email varchar(100) not null,
    empresa_logo varchar(200) not null,
    empresa_enfoque varchar(1000) null,
    empresa_vision varchar(1000) null,
    empresa_mision varchar(1000) null,
    empresa_ciudad varchar (200) null,
    empresa_lema varchar (200) null,
    empresa_about varchar (900) null

);

create table rol(

    rol_id int(5) auto_increment primary key,
    rol_estado int(1) not null,
    rol_nombre varchar(100) not null

);

create table cargopersonal(

    cargopersonal_id int(5) auto_increment primary key,
    cargopersonal_estado int(1) not null,
    cargopersonal_nombre varchar(100) not null

);

create table usuario(
    usuario_id int(5) auto_increment primary key,
    estado_id int(5) not null,
    rol_id int(5) not null,
    usuario_cedula varchar(13) unique not null,
    usuario_nombre varchar(200) not null,
    usuario_apellido varchar(200) not null,
    usuario_correo varchar(100) unique not null,
    usuario_telefono varchar(10) unique not null,
    usuario_celular varchar(10) unique not null,
    usuario_celularjob varchar(10) null,
    usuario_direccion varchar(200) not null,
    usuario_contrasena varchar(255) not null,
    foreign key (estado_id) references estado (estado_id),
    foreign key (rol_id) references rol (rol_id)

);

create table personal(
    personal_id int(5) auto_increment primary key,
    estado_id int(5) not null,
    cargopersonal_id int(5) not null,
    personal_cedula varchar(13) unique not null,
    personal_nombre varchar(200) not null,
    personal_apellido varchar(200) not null,
    personal_correo varchar(100) unique not null,
    personal_telefono varchar(10) unique not null,
    personal_celular varchar(10) unique not null,
    personal_celularjob varchar(10) null,
    personal_direccion varchar(200) not null,
    personal_nombrefoto varchar(200) null,
    foreign key (estado_id) references estado (estado_id),
    foreign key (cargopersonal_id) references cargopersonal (cargopersonal_id)

);

create table categorianoticia(

	categorianoticia_id int(5) auto_increment primary key,
    categorianoticia_descripcion varchar(300) not null,
    categorianoticia_nombre varchar(200) not null

);

create table noticia(

	noticia_id int(20) auto_increment primary key,
    noticia_titulo varchar(300) not null,
    noticia_resumen varchar(500) not null,
    noticia_cuerpo varchar(1000) not null,
    noticia_foto varchar(200) null,
    noticia_fecha date not null,
    noticia_hora time not null,
    usuario_id int(5) not null,
    foreign key (usuario_id) references usuario (usuario_id)

);

create table anuncio(

    anuncio_id int(20) auto_increment primary key,
    anuncio_foto varchar(200) null,
    anuncio_fecha date not null,
    anuncio_hora time not null,
    usuario_id int(5) not null,
    foreign key (usuario_id) references usuario (usuario_id)

);

INSERT INTO `rol` (`rol_id`, `rol_estado`, `rol_nombre`) VALUES (NULL, '1', 'Administrador');
INSERT INTO `estado` (`estado_id`, `estado_estado`, `estado_nombre`) VALUES (NULL, '1', 'Activo');
INSERT INTO `estado` (`estado_id`, `estado_estado`, `estado_nombre`) VALUES (NULL, '1', 'Pasivo');

INSERT INTO `empresa` (`empresa_id`, `empresa_ruc`, `empresa_nombre`, `empresa_direccion`, `empresa_telefono`, `empresa_celular`, `empresa_email`, `empresa_logo`, `empresa_enfoque`, `empresa_vision`, `empresa_mision`,`empresa_ciudad`,`empresa_lema`,`empresa_about`) VALUES (NULL, '1111111111', 'CENTRO PARTICULAR DE EDUCACION INICIAL \"SAN FRANCISCO JAVIER\"', 'LAURO GUERRERO 12-31 Y MERCADILLO', '072560310', '072560310', 'sanfranciscojavierloja@gmail.com', 'logo.jpeg', 'El Instituto Educativo San Francisco Javier, la Escuela Javeriana de Loja: Es una entidad de carácter particular laico sin fines de lucro, puesta al servicio de la culta ciudadanía lojana para atender los niveles pre inicial, inicial y básico hasta el séptimo grado de educación básica. Su objetivo principal es ofrecer a los niños y adolescentes una educación integral con los más altos estándares de calidad, sustentada fundamentalmente en la corriente pedagógica ecologista.

Cumple con su compromiso histórico de brindar una educación acorde con el avance de la ciencia, la técnica, la cultura y las exigencias del entorno; propiciando el desarrollo de la inteligencia, de la capacidad crítica, propositiva y reflexiva, de su identidad nacional, en el marco del reconocimiento de la diversidad cultural y étnica del Ecuador y el fortalecimiento de una personalidad autónoma y solidaria, fundamentada en la vivencia de los valores éticos y morales de acuerdo a los preceptos jesuitas que fueron heredados del máximo representante de la mencionada comunidad SAN FRANCISCO JAVIER

Mantiene una relación transparente con la sociedad a través del sistema de rendición de cuentas ante la familia y la comunidad como ente de cambio a favor de una Educación de Excelencia siempre con el compromiso de trabajar con calidad y calidez.', 'El Instituto Educativo “San Francisco Javier” La escuela Javeriana de Loja, es líder en el proceso formativo del estudiante, en el nivel profesional de su personal y en la generación de innovaciones.\r\nEs la comunidad educativa donde niños y jóvenes quieren ir a estudiar y donde profesores quieren ir a trabajar.\r\nEs el instituto de los padres, de los hijos y de los hijos de los hijos.', 'El Instituto Educativo “San Francisco Javier” es una comunidad educativa particular laica, sin fines de lucro, cuyo aporte a la sociedad lojana y ecuatoriana radica en formar estudiantes íntegros, analíticos, críticos, dotados de profundos valores morales y éticos, y de los conocimientos y destrezas necesarios para un desempeño exitoso en un mundo cambiante.','LOJA','Brindar una Educación de Calidad por medio de la Calidez','El Instituto Educativo San Francisco Javier, la Escuela Javeriana de Loja: Es una entidad de carácter particular laico sin fines de lucro, puesta al servicio de la culta ciudadanía lojana para atender los niveles pre inicial, inicial y básico hasta el séptimo grado de educación básica. Su objetivo principal es ofrecer a los niños y adolescentes una educación integral con los más altos estándares de calidad, sustentada fundamentalmente en la corriente pedagógica ecologista.');

INSERT INTO `usuario` (`usuario_id`, `estado_id`, `rol_id`, `usuario_cedula`, `usuario_nombre`, `usuario_apellido`, `usuario_correo`, `usuario_telefono`,`usuario_celular`,`usuario_celularjob`, `usuario_direccion`, `usuario_nombrefoto`, `usuario_contrasena`) VALUES (NULL, '1', '1', '0704418144', 'Christian Giovanni', 'Bustamante Feijoo', 'chrisysandra1992@gmail.com', '0981105330','0981105330','0981105330', 'Mercadillo y shyris', 'default-profile.png', 'ec43a30fa86452e5db09f030ceb50e62');