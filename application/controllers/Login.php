<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
    }

	public function index()
	{
		$this->load->view('admin/login');
	}

	public function iniciarsesion()
    {

        $this->loginusuario($this->input->post('email'),$this->input->post('contrasena'));
        $this->session->set_flashdata("errorlogin", "El email y/o contraseña son incorrectos");
        redirect("login");
    }

    public function logout()
    {
        $data = array('id' => '', 'nombres' => '');
        $this->session->set_userdata($data);
        $this->session->sess_destroy();
        redirect('login');
    }

    private function loginusuario($correo,$contrasena)
    {
        $datausuario    = $this->model_usuario->login($correo, md5($contrasena));
        if ($datausuario) {
            $datasesionuser = array(
                'id'                         => $datausuario->usuario_id,
                'nombres'                    => $datausuario->usuario_nombre . " " . $datausuario->usuario_apellido,
                'nombrefoto'                 => $datausuario->usuario_nombrefoto,
                'rol'                        => $datausuario->rol_nombre,
                'login'                      => true,
            );
            $this->session->set_userdata($datasesionuser);
            redirect("usuario");
        }
        
    }

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */