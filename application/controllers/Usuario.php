<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('Validatedocidentificacion');
        $this->load->model('model_usuario');
        $this->load->library('Autorizacion');
    }

    public function index()
    {
    	$this->autorizacion->validarlogin();
    	$dataview = array('usuario' => $this->model_usuario->get($this->session->userdata('id')));
    	$this->load->view('layauts/admin/headeradmin');
		$this->load->view('admin/usuario/menu',$dataview);
    }

    public function delete($id)
    {
        $this->autorizacion->validarlogin();
    	$data = array('estado_id' => 2);
    	$respuesta = $this->model_usuario->update($id,$data);
		if ($respuesta == true) {

			redirect('usuario');
		}else{

			redirect('usuario');
		}
    	
    }

    public function activar($id)
    {
    	$this->autorizacion->validarlogin();
        $data = array('estado_id' => 1);
        $respuesta = $this->model_usuario->update($id,$data);
        if ($respuesta == true) {

            redirect('usuario');
        }else{

            redirect('usuario');
        }
        
    }

	public function create()
	{
		$this->autorizacion->validarlogin();
		$this->load->view('layauts/admin/headeradmin');
		$this->load->view('admin/usuario/registrar');
	}

	public function edit($id)
	{
		$this->autorizacion->validarlogin();
		$dataview = array('usuario' => $this->model_usuario->find($id));
		$this->load->view('layauts/admin/headeradmin');
		$this->load->view('admin/usuario/editar',$dataview);
	}

	public function perfil($id)
	{
		$this->autorizacion->validarlogin();
		$dataview = array('usuario' => $this->model_usuario->find($id));
		$this->load->view('layauts/admin/headeradmin');
		$this->load->view('admin/usuario/perfil',$dataview);
	}

	public function save(){

		$this->autorizacion->validarlogin();

		$this->form_validation->set_rules('cedula', 'Cedula', 'required|min_length[10]|max_length[13]|numeric|is_unique[usuario.usuario_cedula]');
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[100]|valid_email|is_unique[usuario.usuario_correo]');
		$this->form_validation->set_rules('telefono', 'Telefono', 'required|min_length[9]|max_length[10]|numeric|is_unique[usuario.usuario_telefono]');
        $this->form_validation->set_rules('celular', 'Celular', 'required|min_length[9]|max_length[10]|numeric|is_unique[usuario.usuario_celular]');
		$this->form_validation->set_rules('direccion', 'Direccion', 'required|min_length[1]|max_length[200]');

		if ($this->form_validation->run() != false) {

			if ($this->validatedocidentificacion->validate($this->input->post('cedula')) === true) {

				$data = array('estado_id' => 1,
							  'rol_id' => 1,
							  'usuario_cedula' => $this->input->post('cedula'),
							  'usuario_nombre' => $this->input->post('nombre'),
							  'usuario_apellido' => $this->input->post('apellido'),
							  'usuario_correo' => $this->input->post('correo'),
							  'usuario_telefono' => $this->input->post('telefono'),
							  'usuario_celular' => $this->input->post('celular'),
							  'usuario_celularjob' => $this->input->post('celular'),
							  'usuario_direccion' => $this->input->post('direccion'),
							  'usuario_nombrefoto' => '',
							  'usuario_contrasena' => md5($this->input->post('correo')),
			                 );

				$respuesta = $this->model_usuario->create($data);

				if ($respuesta === true) {
                     redirect('usuario');
                }else{
                       $this->session->set_flashdata("errorsave","No se a guardado correctamente. Por favor vuelva a intentar.");
                      $this->create();
                }
				
			}else{

				$this->session->set_flashdata("errorcedula","La Cedula es Incorrecta.");
                $this->create();

			}
			
		}else{

			$this->create();

		}
	}

	public function update(){

		$this->autorizacion->validarlogin();

		$usuario = $this->model_usuario->find($this->input->post('id'));

		$uniquecedula = "";
		$uniquecorreo = "";
		$uniquetelefono = "";
		$uniquecelular = "";


		if ($usuario->usuario_cedula != $this->input->post('cedula')) {
			$uniquecedula = "|is_unique[usuario.usuario_cedula]";
		}

		if ($usuario->usuario_correo != $this->input->post('correo')) {
			$uniquecorreo = "|is_unique[usuario.usuario_correo]";
		}

		if ($usuario->usuario_telefono != $this->input->post('telefono')) {
			$uniquetelefono = "|is_unique[usuario.usuario_telefono]";
		}

		if ($usuario->usuario_celular != $this->input->post('celular')) {
			$uniquecelular = "|is_unique[usuario.usuario_celular]";
		}

		$this->form_validation->set_rules('cedula', 'Cedula', 'required|min_length[10]|max_length[13]|numeric'.$uniquecedula);
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[100]|valid_email'.$uniquecorreo);
		$this->form_validation->set_rules('telefono', 'Telefono', 'required|min_length[9]|max_length[10]|numeric'.$uniquetelefono);
        $this->form_validation->set_rules('celular', 'Celular', 'required|min_length[9]|max_length[10]|numeric'.$uniquecelular);
		$this->form_validation->set_rules('direccion', 'Direccion', 'required|min_length[1]|max_length[200]');

		if ($this->form_validation->run() != false) {

			if ($this->validatedocidentificacion->validate($this->input->post('cedula')) === true) {

				$data = array('usuario_cedula' => $this->input->post('cedula'),
							  'usuario_nombre' => $this->input->post('nombre'),
							  'usuario_apellido' => $this->input->post('apellido'),
							  'usuario_correo' => $this->input->post('correo'),
							  'usuario_telefono' => $this->input->post('telefono'),
							  'usuario_celular' => $this->input->post('celular'),
							  'usuario_celularjob' => $this->input->post('celular'),
							  'usuario_direccion' => $this->input->post('direccion')
							  
			                 );

				$respuesta = $this->model_usuario->update($this->input->post('id'),$data);

				if ($respuesta === true) {
                     redirect('usuario');
                }else{
                       $this->session->set_flashdata("errorsave","No se a guardado correctamente. Por favor vuelva a intentar.");
                      $this->edit($this->input->post('id'));
                }
				
			}else{

				$this->session->set_flashdata("errorcedula","La Cedula es Incorrecta.");
                $this->edit($this->input->post('id'));

			}
			
		}else{

			$this->edit($this->input->post('id'));

		}
	}

	public function updateperfil(){

		$this->autorizacion->validarlogin();

		$usuario = $this->model_usuario->find($this->input->post('id'));

		$uniquecedula = "";
		$uniquecorreo = "";
		$uniquetelefono = "";
		$uniquecelular = "";
		$newcontrasena = "";


		if ($usuario->usuario_cedula != $this->input->post('cedula')) {
			$uniquecedula = "|is_unique[usuario.usuario_cedula]";
		}

		if ($usuario->usuario_correo != $this->input->post('correo')) {
			$uniquecorreo = "|is_unique[usuario.usuario_correo]";
		}

		if ($usuario->usuario_telefono != $this->input->post('telefono')) {
			$uniquetelefono = "|is_unique[usuario.usuario_telefono]";
		}

		if ($usuario->usuario_celular != $this->input->post('celular')) {
			$uniquecelular = "|is_unique[usuario.usuario_celular]";
		}

		if (strlen($this->input->post('contrasena')) > 0) {
			$newcontrasena = md5($this->input->post('contrasena'));
		}else{
			$newcontrasena = $usuario->usuario_contrasena;
		}

		$this->form_validation->set_rules('cedula', 'Cedula', 'required|min_length[10]|max_length[13]|numeric'.$uniquecedula);
		$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('apellido', 'Apellido', 'required|min_length[1]|max_length[200]');
		$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[100]|valid_email'.$uniquecorreo);
		$this->form_validation->set_rules('telefono', 'Telefono', 'required|min_length[9]|max_length[10]|numeric'.$uniquetelefono);
        $this->form_validation->set_rules('celular', 'Celular', 'required|min_length[9]|max_length[10]|numeric'.$uniquecelular);
		$this->form_validation->set_rules('direccion', 'Direccion', 'required|min_length[1]|max_length[200]');

		if ($this->form_validation->run() != false) {

			if ($this->validatedocidentificacion->validate($this->input->post('cedula')) === true) {

				$data = array('usuario_cedula' => $this->input->post('cedula'),
							  'usuario_nombre' => $this->input->post('nombre'),
							  'usuario_apellido' => $this->input->post('apellido'),
							  'usuario_correo' => $this->input->post('correo'),
							  'usuario_telefono' => $this->input->post('telefono'),
							  'usuario_celular' => $this->input->post('celular'),
							  'usuario_celularjob' => $this->input->post('celular'),
							  'usuario_direccion' => $this->input->post('direccion'),
							  'usuario_contrasena' => $newcontrasena
							  
			                 );

				$respuesta = $this->model_usuario->update($this->input->post('id'),$data);

				if ($respuesta === true) {
                     redirect('usuario');
                }else{
                       $this->session->set_flashdata("errorsave","No se a guardado correctamente. Por favor vuelva a intentar.");
                      $this->perfil($this->input->post('id'));
                }
				
			}else{

				$this->session->set_flashdata("errorcedula","La Cedula es Incorrecta.");
                $this->perfil($this->input->post('id'));

			}
			
		}else{

			$this->perfil($this->input->post('id'));

		}
	}

}

