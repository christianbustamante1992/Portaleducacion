<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_empresa');
    }

	public function index()
	{
		$dataheader = array('empresa' => $this->model_empresa->getheader());
		$this->load->view('layauts/portal/headerportal',$dataheader);
		$this->load->view('portal/inicio');
		$this->load->view('layauts/portal/footerportal',$dataheader);
	}

	public function about()
	{
		$dataheader = array('empresa' => $this->model_empresa->getheader());
		$dataview = array('empresa' => $this->model_empresa->getbody());
		$this->load->view('layauts/portal/headerportal',$dataheader);
		$this->load->view('portal/about',$dataview);
		$this->load->view('layauts/portal/footerportal',$dataheader);
	}
}
