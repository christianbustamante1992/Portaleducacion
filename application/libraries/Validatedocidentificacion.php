<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Validatedocidentificacion
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function validate($documento)
	{
		$validador = new Tavo\ValidadorEc;
		$resultado = false;

		if (($validador->validarCedula((string)$documento)) || ($validador->validarRucPersonaNatural((string)$documento)) || ($validador->validarRucSociedadPrivada((string)$documento)) || ($validador->validarRucSociedadPublica((string)$documento))) {
			$resultado = true;
		}

		return $resultado;
	}

	

}

/* End of file Validatedocidentificacion.php */
/* Location: ./application/libraries/Validatedocidentificacion.php */
