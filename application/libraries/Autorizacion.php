<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autorizacion
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->library('session');
	}

	public function validarlogin()
    {
       if ($this->ci->session->userdata('rol') != "Administrador") {

        redirect('login/logout');

       }
      
    }

	

}

/* End of file Autorizacion.php */
/* Location: ./application/libraries/Autorizacion.php */
