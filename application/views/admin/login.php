<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<meta charset="utf-8">
	<title>CENTRO PARTICULAR DE EDUCACION INICIAL "SAN FRANCISCO JAVIER"</title>

	<!-- Site favicon -->
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>

	<script src="<?php echo base_url();?>assets/admin/vendors/scripts/script.js"></script>

	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119386393-1');
	</script>

<body>
	<div class="login-wrap customscroll d-flex align-items-center flex-wrap justify-content-center pd-20">
		<div class="login-box bg-white box-shadow pd-30 border-radius-5">
			<img src="<?php echo base_url();?>assets/admin/vendors/images/login-img.png" alt="login" class="login-img">
			<h2 class="text-center mb-30">Login</h2>
			<?php if($this->session->flashdata("errorlogin")):?>
              <div class="alert alert-danger">
                <p><?php echo $this->session->flashdata("errorlogin")?></p>
              </div>
            <?php endif; ?>
			<form method="POST" action="<?php echo base_url();?>login/iniciarsesion/">
				<div class="input-group custom input-group-lg">
					<input type="email" class="form-control" placeholder="Email" required="true" name="email" maxlength="100" >
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-at" aria-hidden="true"></i></span>
					</div>
				</div>
				<div class="input-group custom input-group-lg">
					<input type="password" class="form-control" placeholder="**********" name="contrasena" required="true" >
					<div class="input-group-append custom">
						<span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
					</div>
				</div>
			
				<div class="row">
					
					<div class="col-sm-12">
						<div class="input-group">
							<!--
								use code for form submit
								<input class="btn btn-outline-primary btn-lg btn-block" type="submit" value="Sign In">
							-->
							<input class="btn btn-outline-primary btn-lg btn-block" type="submit" value="Iniciar Sesion">
						</div>
					</div>
					
				</div>
			
			</form>
		</div>
	</div>
	
</body>
	
</html>
