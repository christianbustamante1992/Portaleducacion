<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				
					<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<center><h3>Usuarios</h3></center><br>
							<a href="<?php echo base_url();?>usuario/create" class = "btn btn-success"><i class="icon-copy ion-plus-round"></i> Añadir</a><br><br>
							<div>
						<table class="stripe hover data-table-export nowrap" >
								<thead>
									<tr>
										<th>Cedula</th>
										<th class="table-plus datatable-nosort">Nombre</th>
										<th>Correo</th>
										<th>Telefono</th>
										<th>Direccion</th>
										<th>Estado</th>
										<th>Opciones</th>
										
									</tr>
								</thead>
								<tbody>
									<?php  
								        foreach ($usuario as $key) {
								    ?>
									<tr>
									<td class="table-plus"><?php echo $key->usuario_cedula; ?></td>
									<td><?php echo $key->usuario_nombre." ".$key->usuario_apellido; ?></td>
									<td><?php echo $key->usuario_correo; ?></td>
									<td><?php echo $key->usuario_telefono." ".$key->usuario_celular; ?></td>
									<td><?php echo $key->usuario_direccion; ?></td>
									<td><?php echo $key->estado_nombre; ?></td>
									<td>
										<a href="<?php echo base_url(); ?>usuario/edit/<?php echo $key->usuario_id; ?>"><i class="icon-copy fa fa-edit text-info" aria-hidden="true"></i></a>
										<?php if($key->estado_id === '1'){ ?>
										<a href="<?php echo base_url(); ?>usuario/delete/<?php echo $key->usuario_id; ?>"><i class="icon-copy fa fa-trash text-danger" aria-hidden="true"></i></a>
									<?php }else{ ?>

										<a href="<?php echo base_url(); ?>usuario/activar/<?php echo $key->usuario_id; ?>"><i class="icon-copy fa fa-check text-success" aria-hidden="true"></i></a>

									<?php } ?>
									</td>
								</tr>
								<?php  
							        }
							    ?>
								</tbody>
							</table>
						</div>
								
							</div>
					</div>

			</div>
		</div>
