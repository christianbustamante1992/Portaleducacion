<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				
					<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
						<center><h3>Registrar Usuario</h3></center><br>

						<div>
							 
                  			 <span style="color:red;"><?php if($this->session->flashdata("errorsave")):?>
                              <?php echo $this->session->flashdata("errorsave");?>
                             <?php endif; ?></span>
  	          	             
                             
						</div>

						<?php
				        $atributos = array('method' => 'POST', 'class' => 'form-horizontal');
				        echo form_open_multipart('usuario/save',$atributos);
			            ?>

			            <div class="form-group">
							<label>Cedula <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'cedula',
				                               'autofocus' => 'true',
		                                       'value' => set_value('cedula'),
				                               'maxlength' => '13',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su cèdula',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php if($this->session->flashdata("errorcedula")):?>
                              <?php echo $this->session->flashdata("errorcedula");?>
                             <?php endif; ?></span>
                             <span style="color:red;"><?php echo form_error('cedula'); ?></span>

						</div>
						<div class="form-group">
							<label>Nombre <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'nombre',
				                               'value' => set_value('nombre'),
				                               'maxlength' => '200',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su nombre',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php echo form_error('nombre'); ?></span>
						</div>
						<div class="form-group">
							<label>Apellido <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'apellido',
				                               'value' => set_value('apellido'),
				                               'maxlength' => '200',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su apellido',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php echo form_error('apellido'); ?></span>
						</div>
						<div class="form-group">
							<label>Correo <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'email',
				                               'name' => 'correo',
				                               'value' => set_value('correo'),
				                               'maxlength' => '100',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su correo',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
						</div>
						<div class="form-group">
							<label>Telefono <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'telefono',
				                               'value' => set_value('telefono'),
				                               'maxlength' => '10',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su telefono',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php echo form_error('telefono'); ?></span>
						</div>
						<div class="form-group">
							<label>Celular <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'celular',
				                               'value' => set_value('celular'),
				                               'maxlength' => '10',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su celular',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php echo form_error('celular'); ?></span>
						</div>
						<div class="form-group">
							<label>Direccion <span style="color: red;">*</span></label>
							<?php
				            $atributos = array('type' => 'text',
				                               'name' => 'direccion',
				                               'value' => set_value('direccion'),
				                               'maxlength' => '200',
				                               'class' => 'form-control',
				                               'placeholder' => 'Ingrese su direccion',
				                               'required' => 'true'
				                              );
				            echo form_input($atributos);
				            ?>
				            <span style="color:red;"><?php echo form_error('direccion'); ?></span>
						</div>
						<div>
							<center>
								<?php

					            echo form_submit('submit', 'Registrar', 'class="btn btn-primary"');
					            ?>
								<a href="<?php echo base_url(); ?>usuario" class="btn btn-danger">Cancelar</a>
					            <?php echo form_close(); ?>
				            </center>
			            </div>
					</div>
					
				
				
			</div>
			
		</div>
	</div>