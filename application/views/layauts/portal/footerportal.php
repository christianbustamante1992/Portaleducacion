<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<footer class="probootstrap-footer probootstrap-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="probootstrap-footer-widget">
                <h3>Sobre Nosotros</h3>
                <p style="text-align: justify;"><?php echo $empresa->empresa_about; ?></p>
                <h3>Social</h3>
                <ul class="probootstrap-footer-social">
                    <li><a href="https://www.facebook.com/institutosanfranciscojavier/" target="_blank"><i class="icon-facebook"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-md-push-1">
              <div class="probootstrap-footer-widget">
                <h3>Links</h3>
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Courses</a></li>
                  <li><a href="#">Teachers</a></li>
                  <li><a href="#">News</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4">
              <div class="probootstrap-footer-widget">
                <h3>Información de Contacto</h3>
                <ul class="probootstrap-contact-info">
                  <li><i class="icon-location2"></i> <span><?php echo $empresa->empresa_direccion; ?></span></li>
                  <li><i class="icon-mail"></i><span><?php echo $empresa->empresa_email; ?></span></li>
                  <li><i class="icon-phone2"></i><span><?php echo $empresa->empresa_telefono; ?></span></li>
                </ul>
              </div>
            </div>
           
          </div>
          <!-- END row -->
          
        </div>

        <div class="probootstrap-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-8 text-left">
                <p>Copyright &copy; 2018 <a href="https://www.sofpymes.com/">PuntoPymes</a>. All Rights Reserved.</p>
              </div>
              <div class="col-md-4 probootstrap-back-to-top">
                <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>