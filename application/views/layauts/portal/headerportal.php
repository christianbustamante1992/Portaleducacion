<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CENTRO PARTICULAR DE EDUCACION INICIAL "SAN FRANCISCO JAVIER"</title>
        
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portal/css/styles-merged.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portal/css/style.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portal/css/custom.css">

    <script src="<?php echo base_url(); ?>assets/portal/js/scripts.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/portal/js/main.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/portal/js/custom.js"></script>

  </head>
  <body>

      
    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
      
      <div class="probootstrap-header-top">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 probootstrap-top-quick-contact-info">
              <span><i class="icon-location2"></i><?php echo $empresa->empresa_direccion; ?></span>
              <span><i class="icon-phone2"></i><?php echo $empresa->empresa_telefono; ?></span>
              <span><i class="icon-mail"></i><?php echo $empresa->empresa_email; ?></span>
                            <span><a href="https://www.facebook.com/institutosanfranciscojavier/" target="_blank"><i class="icon-facebook2"></i></a></span>
                          </div>
            <div class="col-lg-3 col-md-3 col-sm-3 probootstrap-top-social">
              <ul>
                <li><a href="<?php echo base_url(); ?>login" style="color: black;" target="_blank">Iniciar Sesion</a></li>
                
                
              </ul>
            </div>
          </div>
        </div>
      </div>
      <nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
          <div class="navbar-header">
            <div class="btn-more js-btn-more visible-xs">
              <a href="#"><i class="icon-dots-three-vertical "></i></a>
            </div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
           
          </div>
          <br>
          <div>
            <ul class="nav navbar-nav navbar-center">
              <li>
               <img src="<?php echo base_url(); ?>uploads/logo.jpg" width="100" height="100">
             </li>
             <li>
            
          </li>
            </ul>
           
          </div>
         
          <div id="navbar-collapse" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo base_url(); ?>">Inicio</a></li>
              <li><a href="<?php echo base_url(); ?>inicio/about">Quienes Somos</a></li>
              <li><a href="teachers.html">Noticias</a></li>           
              <li><a href="contact.html">Contacto</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <br>    

    </div>
    <!-- END wrapper -->
    

  

  </body>
</html>