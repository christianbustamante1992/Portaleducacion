<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<meta charset="utf-8">
	<title>CENTRO PARTICULAR DE EDUCACION INICIAL "SAN FRANCISCO JAVIER"</title>

	<!-- Site favicon -->
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/css/responsive.dataTables.css">

	<script src="<?php echo base_url();?>assets/admin/vendors/scripts/script.js"></script>

	
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/dataTables.responsive.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/responsive.bootstrap4.js"></script>
	<!-- buttons for Export datatable -->
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/dataTables.buttons.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/buttons.bootstrap4.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/buttons.print.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/buttons.html5.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/buttons.flash.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/pdfmake.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/src/plugins/datatables/media/js/button/vfs_fonts.js"></script>
	<script>
		$('document').ready(function(){
			$('.data-table').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Search"
				},
			});
			$('.data-table-export').DataTable({
				/*scrollCollapse: false,*/
				autoWidth: false,
				//responsive: true,
				scrollX: true,
				/*columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],*/
				/*"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],*/
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Buscar"
				},
				dom: 'Bfrtip',
				buttons: [
				'copy', 'csv', 'pdf', 'print'
				]
			});
			var table = $('.select-row').DataTable();
			$('.select-row tbody').on('click', 'tr', function () {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				}
				else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
			var multipletable = $('.multiple-select-row').DataTable();
			$('.multiple-select-row tbody').on('click', 'tr', function () {
				$(this).toggleClass('selected');
			});
		});
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119386393-1');
	</script>
	
	<body>
		<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="index.php">
					<img src="vendors/images/logo.png" alt="" class="mobile-logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name"><?php echo $this->session->userdata('nombres'); ?></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="<?php echo base_url(); ?>usuario/perfil/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-user-md" aria-hidden="true"></i> Perfil</a>
						<a class="dropdown-item" href=""><i class="fa fa-question" aria-hidden="true"></i> Ayuda</a>
						<a class="dropdown-item" href="<?php echo base_url(); ?>login/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar Sesion</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="index.php">
				<img src="<?php echo base_url();?>uploads/logo.jpg" alt="" width="40" height="40">
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					
					<li>
						<a href="<?php echo base_url();?>usuario" class="dropdown-toggle no-arrow">
							<span class="fa fa-users"></span><span class="mtext">Usuarios</span>
						</a>
					</li>

					<li>
						<a href="<?php echo base_url();?>personal" class="dropdown-toggle no-arrow">
							<span class="fa fa-child"></span><span class="mtext">Personal</span>
						</a>
					</li>

					<li>
						<a href="<?php echo base_url();?>empresa" class="dropdown-toggle no-arrow">
							<span class="fa fa-mortar-board"></span><span class="mtext">Unidad Educativa</span>
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url();?>noticia" class="dropdown-toggle no-arrow">
							<span class="fa fa-twitch"></span><span class="mtext">Noticias</span>
						</a>
					</li>	

					<li>
						<a href="<?php echo base_url();?>anuncio" class="dropdown-toggle no-arrow">
							<span class="fa fa-object-ungroup"></span><span class="mtext">Anuncios</span>
						</a>
					</li>

					<li>
						<a href="<?php echo base_url();?>cargopersonal" class="dropdown-toggle no-arrow">
							<span class="fa fa-black-tie"></span><span class="mtext">Cargos de Personal</span>
						</a>
					</li>
					
					
				</ul>
			</div>
		</div>
	</div>
	
	</body>
</html>
