<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="probootstrap-section">
        <div class="container">
          
          
          <div class="col-md-push-1">
            <h2>Quienes Somos</h2>
            <p style="text-align: justify;">El Instituto Educativo San Francisco Javier, la Escuela Javeriana de Loja: Es una entidad de carácter particular laico sin fines de lucro, puesta al servicio de la culta ciudadanía lojana para atender los niveles pre inicial, inicial y básico hasta el séptimo grado de educación básica. Su objetivo principal es ofrecer a los niños y adolescentes una educación integral con los más altos estándares de calidad, sustentada fundamentalmente en la corriente pedagógica ecologista.</p>

            <p style="text-align: justify;">

Cumple con su compromiso histórico de brindar una educación acorde con el avance de la ciencia, la técnica, la cultura y las exigencias del entorno; propiciando el desarrollo de la inteligencia, de la capacidad crítica, propositiva y reflexiva, de su identidad nacional, en el marco del reconocimiento de la diversidad cultural y étnica del Ecuador y el fortalecimiento de una personalidad autónoma y solidaria, fundamentada en la vivencia de los valores éticos y morales de acuerdo a los preceptos jesuitas que fueron heredados del máximo representante de la mencionada comunidad SAN FRANCISCO JAVIER. </p>

<p style="text-align: justify;">

Mantiene una relación transparente con la sociedad a través del sistema de rendición de cuentas ante la familia y la comunidad como ente de cambio a favor de una Educación de Excelencia siempre con el compromiso de trabajar con calidad y calidez.</p>
          </div>

          <div class="col-md-push-1">
            <h2>Visión</h2>
            <p style="text-align: justify;"><?php echo $empresa->empresa_vision; ?></p>
          </div>

          <div class="col-md-push-1">
            <h2>Misión</h2>
            <p style="text-align: justify;"><?php echo $empresa->empresa_mision; ?></p>
          </div>
          

        </div>
      </section>