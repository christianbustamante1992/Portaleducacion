<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_empresa extends CI_Model {

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getheader()
    {
    	$consulta = $this->db->query("SELECT empresa_direccion,empresa_telefono,empresa_celular,empresa_email,empresa_about FROM empresa;");
        $resultado = $consulta->row();
        return $resultado;
    }

    public function getbody()
    {
        $consulta = $this->db->query("SELECT empresa_enfoque,empresa_vision,empresa_mision FROM empresa;");
        $resultado = $consulta->row();
        return $resultado;
    }

}

/* End of file Model_empresa.php */
/* Location: ./application/models/Model_empresa.php */