<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * summary
 */
class Model_usuario extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
     	parent::__construct();
        $this->load->database();  
    }

   public function get($id)
   {
       $consulta = $this->db->query("SELECT * FROM usuario,estado,rol WHERE usuario_id != '$id' AND usuario.estado_id = estado.estado_id AND usuario.rol_id = rol.rol_id;");
        $resultado = $consulta->result();
        return $resultado;
   }

    public function create($data)
    {
        $this->db->trans_begin();
        $consulta = $this->db->insert('usuario', $data);
        if ($this->db->trans_status() === false) {
            # code...
             $this->db->trans_rollback();      
             return false;    
        }else{
            $this->db->trans_commit();    
         return true;  
        }
    }

    public function update($id,$data)
    {
        $this->db->trans_begin();
        $this->db->where('usuario_id',$id);
        $this->db->update('usuario',$data);
        if ($this->db->trans_status() === false) {
            # code...
             $this->db->trans_rollback();      
             return false;    
        }else{
            $this->db->trans_commit();    
         return true;  
        }
    }

    public function find($id)
    {
        
        $consulta = $this->db->query("SELECT * FROM usuario WHERE usuario_id = '$id';");
        $resultado = $consulta->row();
        return $resultado;
    }

    

     public function login($correo,$contrasena)
    {
        
        $consulta = $this->db->query("SELECT * FROM usuario,estado,rol WHERE usuario_correo = '$correo' AND usuario_contrasena = '$contrasena' AND usuario.estado_id = 1 AND usuario.rol_id = rol.rol_id;");
        $resultado = $consulta->row();
        return $resultado;
    }
     
}

?>